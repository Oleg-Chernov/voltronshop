(function ($) {
	$(function () {
		$(document).ready(function () {
			$('.carousel').carousel();
		});
		
		$('.button-collapse').sideNav();

		$('input.autocomplete-city').autocomplete({
			data: {
				"Санкт-Петербург": null,
				"Москва": null,
				"Нижний-Новгород": null,
				"Пенза": null,
				"Перьмь": null,
				"Саратов": null,
				"Краснодар": null,
				"Тюмень": null,
				"Иркутск": null,
				"Барнаул": null,
				"Ижевск": null,
				"Хабаровск": null,
				"Ярославль": null,
				"Владивасток": null,
				"Махачкала": null,
				"Томс": null,
				"Оренбург": null,
				"Кемерово": null,
				"Новокузнецк": null,
				"Рязань": null,
				"Астрахань": null,
				"Липецк": null,
				"Чебоксары": null,
				"Балашиха": null,
			},
			limit: 50, // The max amount of results that can be shown at once. Default: Infinity.
			onAutocomplete: function (val) {
				// Callback function when value is autcompleted.
			},
			minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
		});
	});
})(jQuery);


//MENU
document.querySelector('.lighten-1').innerHTML = '' +
	'<div class="nav-wrapper container">' +
	'<a id="logo-container" href="http://voltronshop.ru" class="brand-logo">Вольтрон</a>' +
	'<ul class="right hide-on-med-and-down">' +
	'<li><a href="index.html">Главная</a></li>' +
	'<li><a href="catigories.html">Категории</a></li>' +
	'</ul>' +
	'<ul id="nav-mobile" class="side-nav">' +
	'<li><a href="index.html">Главная</a></li>' +
	'<li><a href="catigories.html">Категории</a></li>' +
	'</ul>' +
	'<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>' +
	'</div>';

//FOOTER
document.querySelector('.page-footer').innerHTML = '' +
	'<div class="row">' +
	'<div class="col l5 m6 s12">' +
	'<h5 class="white-text">Мы в соц.сетях</h5>' +
	'<ul>' +
	'<li><a class="white-text" href="https://vk.com/voltron_shop" target="_blank">Вконтакте</a></li>' +
	'<li><a class="white-text" href="#!">Google+</a></li>' +
	'</ul>' +
	'</div>' +
	'<div class="col l5 m6 s12">'+
	'<h5 class="white-text">Информация</h5>' +
	'<ul>' +
	'<li><a class="white-text" href="etc/delivery.html">Информация о доставке</a></li>' +
	'<li><a class="white-text" href="etc/buy.html">Информация об оплате</a></li>' +
	'</ul>' +
	'</div>'+
	'</div>'

