var product = window.location.search.toString().split('-');

function listProduct(category) {
	//вывод всех радиодетале
	document.body.innerHTML += '<script src="catigories/' + category + '/price.js"></script>'
}

function cardProduct(set) {
	//добовляет продукт в категории
	document.querySelector('.section').innerHTML += '<div class="card">' +
		'<div class="card-image">' +
		'<img src="image/' + set.image + '">' +
		'<span class="card-title">' + set.name + '</span>' +
		'<a class="btn-floating halfway-fab waves-effect waves-light red" href="' + set.link + '"><i class="material-icons">shopping_basket</i></a>' +
		'</div>' +
		'<div class="card-content">' +
		'<p class="price-product">Цена <b>' + set.price+ '</b> рублей</p><hr>'+
		'<p>' + set.description + '</p>' +
		'</div>' +
		'</div>';
}

function addProduct(product) {
	
	var availabilityHtml = ''
	
	if(product.availability == true) {
		availabilityHtml = '<p class="text-green"><i class="material-icons">done</i>Товар в наличии</p>';
	}
	
	if(product.availability == false) {
		availabilityHtml = '<p class="text-red"><i class="material-icons">error</i>Товара нет на складе. Предзаказ</p>'	
	}
	
	document.querySelector('.product-item').innerHTML += ' <div class="col s12 m6 l6">' +
		'<img class="responsive-img materialboxed" src="image/' + product.image + '" alt="' + product.altImage + '" >' +
		'</div>' +
		'<div class ="col s12 m6 l6">' +
		'<h2>' + product.name + '</h2>' +
		'<p>' + product.description + '</p>' +
		'<hr>' +
		availabilityHtml +
		'<p> <b> Цена: </b> <span id="productPrice">' + product.price + '</span> руб.</p>';
	document.querySelector('span#productPrice').innerHTML = product.price;
}

function topProduct(set) {
	document.querySelector('.top-item').innerHTML += '' +
		'<div class="col s12 m6 l4">' +
		'<div class="card">' +
		'<div class="card-image">' +
		'<img src="' + set.image + '">' +
		'<span class="card-title">' + set.name + '</span>' +
		'<a class="btn-floating halfway-fab waves-effect waves-light red" href="' + set.link + '"><i class="material-icons">shopping_basket</i></a>' +
		'</div>' +
		'<div class="card-content">' +
		'<p>' + set.description + '</p>' +
		'</div>' +
		'</div>' +
		'</div>';
}

function infoBlock(set) {
	document.querySelector('.info-block').innerHTML += '' +
		      	'<div class = "col s12 m12 l12">'+
		      	'<div class = "card blue-grey lighten-5 black-text">'+
		      	'<div class = "card-content">'+
		      	'<span class = "card-title">' + set.header + '</span>'+
				'<p>' + set.description + '</p>'+ 
				'</div>'+
				'<div class = "card-action">'+
		      	'<a class="waves-effect black-text grey lighten-4 btn" href="' + set.link + '">' + set.linkText + '</a>'+
				'</div>'+
				'</div>'+
				'</div>';
}
