
document.querySelector('#order').onclick = function() {
	document.querySelector('.order').style.display = 'block';
}

function validEmail(email) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
		return true;
	}
	return false;
}

function validPhone(phone) {
	var phoneno = /^\d{10}$/;
	if (phone.match(/^([+]?[0-9\s-\(\)]{3,25})*$/i)) {
			return true;
		} else {
			return false;
		}
}

document.querySelector('#dost').onclick = function() {
	document.querySelector('#instruction-dost').innerHTML = ' <a href="etc/buy.html#dost">Примечание по оплате</a>'
}

document.querySelector('#bitcoin').onclick = function() {
	document.querySelector('#instruction-bitcoin').innerHTML = ' <a href="etc/buy.html#crypto">Примечание по оплате</a>'
}

 function check() { //Проверка способов доставки
 	var rarr = document.getElementsByName("group1");
	 
 	if (rarr[0].checked) { //courier
		var productPrice = Number(document.querySelector('span#productPrice').innerHTML);
 		productPrice += 200;
 		document.querySelector('#total').innerHTML = productPrice;
 	}
	 
 	if (rarr[1].checked) { //self-expression
		var productPrice = Number(document.querySelector('span#productPrice').innerHTML);
 		productPrice += 20;
 		document.querySelector('#total').innerHTML = productPrice;
 	}
 }

function finish() {
	var name = document.querySelector('#name');
	var surname = document.querySelector('#surname');
	var pstronymic = document.querySelector('#pstronymic');
	var email = document.querySelector('#email');
	var phone = document.querySelector('#phone');
	var city = document.querySelector('#city');
	var adress = document.querySelector('#adress');
	var index = document.querySelector('#index');
	
	if(name.value == '') {
		Materialize.toast('Пожалуйста, укажите Имя', 1000, 'rounded')
	} else {
		if(surname.value == '') {
			Materialize.toast('Пожалуйста, укажите Фамилию', 1000, 'rounded')
		} else {
			if(pstronymic == '') {
				Materialize.toast('Пожалуйста, укажите отчество', 1000, 'rounded')
			} else {
				if(validEmail(email.value) == false) {
					Materialize.toast('Пожалуйста, введите правильный Email', 1000, 'rounded')
				} else {
					if(validPhone(phone.value) == false) {
						Materialize.toast('Пожалуйста, введите правильный номер телефона', 1000, 'rounded')
					} else {
						if(city.value == '') {
							Materialize.toast('Пожалуйста, введите Город', 1000, 'rounded')
						} else {
							if(adress.value == '') {
								Materialize.toast('Пожалуйста, введите адрес', 1000, 'rounded');
							} else {
								if(index.value == '' | index.value.length < 6) {
									Materialize.toast('Пожалуйста, укажите корректный индекс', 1000, 'rounded')
								} else {
									//тут отправка данных на сервер
									var xhr = new XMLHttpRequest();

									var params = 'name=' + encodeURIComponent(name) +
										'&surname=' + encodeURIComponent(surname);

									xhr.open("POST", 'submit.php' + params, true);
									xhr.send();
								}
							}
						}
					}
				}
			}
		}
	}
}

