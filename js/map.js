function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 8,
          center: {lat: -30, lng: 80.644}
        });
        var geocoder = new google.maps.Geocoder();

        document.getElementById('order-btn').addEventListener('click', function() {
          geocodeAddress(geocoder, map);
        });
      }

      function geocodeAddress(geocoder, resultsMap) {
        var city = document.getElementById('city').value;
		var index = document.getElementById('index').value;
        geocoder.geocode({'address': city + " " + index}, function(results, status) {
          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
			  console.log(results[0].geometry.location + 1)
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }