function m1(){
	addProduct({
			name: 'Сосновая канифоль 20г',
			description: 'Сосновая канифоль, отлично подходит в качестве нейтрального флюса',
			image: 'materials/canifol.jpg',
			altImage: 'Сосновая канифоль',
			price: 100,
			availability: true
		})
}